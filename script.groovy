def buildJar() {
    echo "building the application..."
    // sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'Dockerhub-repository', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t salman14/demo-app:jma-2.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push salman14/demo-app:jma-2.0'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this