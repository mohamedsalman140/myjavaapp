FROM openjdk:11-jre-slim
COPY ./target/java-maven-app-1.1.0-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
ENTRYPOINT [ "java", "-jar", "maven-app-1.0-SNAPSHOT.jar" ] 
